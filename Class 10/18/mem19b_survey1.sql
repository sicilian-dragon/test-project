--mem19b_survey.sql
--IT 325.01 -- Web Application Development
--Database Survey Generator 1
-- My Survey Generator

--Commands to import SQL file locally
--\i 'C:/Users/Marlon Miller/OneDrive/Documents/Fall_2021/ITC/mem19b_survey1.sql'
--https://dba.stackexchange.com/questions/29767/permission-denied-in-file-trying-to-import

-- Database Schema and Data
-- 
-- Y C: Schema and
-- 
-- Y relevant constraints
-- Y insert statements for each table and selects 'proving' it works
-- Y descriptive statistics for all-the-things, e.g. counts of total surveys, active surveys, users (by survey, by question, etc), questions (by survey, by user, etc)
-- Y Likert questions
-- Y sample data
-- Y 20 questions
-- Y 5 surveys (at least 4 questions each)
-- Y 20 users (15 do 1 survey)
-- Y 20 users (5 do 2 surveys)
-- Y 2 admins
-- 
-- Y B: "C" plus
-- 
-- Y +True/False questions
-- Y +10 questions
-- Y +5 surveys (at least 4 questions each)
-- Y +10 users (15 do 1 survey)
-- Y +10 users (10 do 2 surveys)
-- Y +10 users (5 do 3 surveys)
-- Y +1 admin
--  
-- Y A: "B" plus
--  
-- Y +Multiple Choice
-- Y +10 questions
-- Y +5 surveys (at least 4 questions each)
-- Y +10 users (15 do 1 survey)
-- Y +10 users (15 do 2 surveys)
-- Y +10 users (5 do 3 surveys)
-- Y +10 users (5 do 4 surveys)
-- Y +1 admin

drop database if exists mem19b_survey;
CREATE DATABASE mem19b_survey encoding 'UTF-8';
\c mem19b_survey;

-- Questions
-- Y relevant constraints
drop table if exists question;
CREATE TABLE question (
id serial primary key,
text text,
question_type varchar(2)
);

-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
-- Y Likert questions
-- Y 20 questions
-- Y +10 questions
-- Y +10 questions
-- Y +Multiple Choice
-- Y +True/False questions
insert into question (id, text, question_type) values
(1, 'I love Purple', 'L4'),
(2, 'I love orange', 'L5'),
(3, 'Peanuts are not nuts!', 'TF'),
(4, 'If multiple CSS rules apply to the same element, what will determine which rule wins?', 'MC'),
(5, 'I feel comfortable asking my professors for help', 'L4'),
(6, 'This professor did a good job at teaching the material for the course', 'L5'),
(7, 'Emus can fly', 'TF'),
(8, 'What is a CSS Reset?', 'MC'),
(9, 'I think Ford makes the best cars', 'L4'),
(10, 'I feel safe driving my Ford', 'L5'),
(11, 'Emus cant fly', 'TF'),
(12, 'Which of these statements is true?', 'MC'),
(13, 'I think Chevy makes the best cars', 'L4'),
(14, 'I feel safe driving my Chevy', 'L5'),
(15, 'ACU has 3,500 students enrolled', 'TF'),
(16, 'Which setting defines a pure blue color at full brightness and half opacity', 'MC'),
(17, 'I think Toyota makes the best cars', 'L4'),
(18, 'I feel safe driving my Toyota', 'L5'),
(19, 'ACUs school colors are purple, yellow', 'TF'),
(20, 'What year was ACU founded', 'MC'),
(21, 'I love green', 'L4'),
(22, 'ACU has a good football team', 'L5'),
(23, 'Phils last name is Shoobert', 'TF'),
(24, 'Which position option would you use to place an element in a specific location regardles of screen size or scrolling', 'MC'),
(25, 'ACU has a good basketball team', 'L4'),
(26, 'I love the Bean', 'L5'),
(27, 'Phils last name is Shubert', 'TF'),
(28, 'When using a flexbox layout, which rule sets the flow of the main axis to horizontal', 'MC'),
(29, 'I enjoyed my high school experience', 'L4'),
(30, 'I enjoyed my college/university experience', 'L5'),
(31, 'Monkeys have tails', 'TF'),
(32, 'What is the difference between a function and a method', 'MC'),
(33, 'I love Darkness', 'L4'),
(34, 'I love the rain', 'L5'),
(35, 'Monkeys dont have tails', 'TF'),
(36, 'How do you add an element created using createElement() to the DOM?', 'MC'),
(37, 'I love cold weather', 'L4'),
(38, 'I feel safe in my school', 'L5'),
(39, 'Apple was started by Steven Bills', 'TF'),
(40, 'Why is the best-practice to place objects inside constants?', 'MC');

-- Y relevant constraints
drop table if exists choices_type;
CREATE TABLE choices_type (
  id serial primary key,
  question_type varchar(2),
  choice varchar(2)
);

-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
-- Y True/False Answers
insert into choices_type (id,question_type,choice) values
(1,'TF','T'),
(2,'TF','F'),

(3,'L4', '1'),
(4,'L4', '2'),
(5,'L4', '3'),
(6,'L4', '4'),

(7,'L5', '1'),
(8,'L5', '2'),
(9,'L5', '3'),
(10,'L5', '4'),
(11,'L5', '5');

-- Y relevant constraints
drop table if exists choice_mc; -- choices for Multiple Choise Questions
CREATE TABLE choice_mc (
  id serial primary key,
  id_q_n int, -- Question number from table question
  choices text -- change to choice
);

-- Y descriptive statistics for all-the-things, e.g. counts of total surveys, active surveys, users (by survey, by question, etc), questions (by survey, by user, etc)
-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
--Multiple Choice Question Choices with 4 different values always
-- + Multiple Choice 'MC' Answers
insert into choice_mc (id,id_q_n, choices) values
(1,4,'All of the above'),
(2,4,'None correct'),
(3,4,'Last rule'),
(4,4,'First rule'),

(5,8,'I dont know '),
(6,8,'When CSS turns off and on'),
(7,8,'None of the above'),
(8,8,'CSS is evil'),

(9,12,'CSS is the best'),
(10,12,'SITC is the greatest department'),
(11,12,'Earth is in space'),
(12,12,'The Sun is the biggest Star in the Universe'),

(13,16,'Change color'),
(14,16,'Color settings'),
(15,16,'All of the above'),
(16,16,'None of the above'),

(17,20,'2020'),
(18,20,'1943'),
(19,20,'1926'),
(20,20,'1906'),

(21,24,'I dont know '),
(22,24,'When CSS turns off and on'),
(23,24,'None of the above'),
(24,24,'CSS is evil'),

(25,28,'I have no idea'),
(26,28,'Answers are in linkedIn Course'),
(27,28,'Quizzes are important'),
(28,28,'All of the above'),

(29,32,'Function and methods are the same'),
(30,32,'No such thing as Function'),
(31,32,'Both of them are the same'),
(32,32,'None of the above'),

(33,36,'Insert the element into the function'),
(34,36,'Copy and paste'),
(35,36,'Its impossible'),
(36,36,'All of the above'),

(37,40,'you cant replace the object (or collection) that was assigned '),
(38,40,'Make objects private'),
(39,40,'Make objects public'),
(40,40,'None of the above');

-- Y relevant constraints
drop table if exists response;
CREATE TABLE response (
  user_id int,
  question_id int,
  survey_id int,
  answer char(2)
);

-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
-- Y 5 surveys (at least 4 questions each)
-- Y +5 surveys (at least 4 questions each)
-- Y +5 surveys (at least 4 questions each)
insert into response (user_id, survey_id, question_id, answer) values
(1,1,1,'4'),
(1,1,2,'3'),
(1,1,3,'T'),
(1,1,4,'C'),
(2,2,5,'2'),
(2,2,6,'5'),
(2,2,7,'F'),
(2,2,8,'A'),
(3,3,9,'4'),
(3,3,10,'5'),
(3,3,11,'F'),
(3,3,12,'D'),
(4,4,13,'2'),
(4,4,14,'3'),
(4,4,15,'T'),
(4,4,16,'B'),
(5,5,17,'4'),
(5,5,18,'5'),
(5,5,19,'T'),
(5,5,20,'A'),
(6,6,21,'4'),
(6,6,22,'1'),
(6,6,23,'T'),
(6,6,24,'C'),
(7,7,25,'4'),
(7,7,26,'3'),
(7,7,27,'F'),
(7,7,28,'D'),
(8,8,29,'2'),
(8,8,30,'1'),
(8,8,31,'T'),
(8,8,32,'A'),
(9,9,33,'2'),
(9,9,34,'1'),
(9,9,35,'T'),
(9,9,36,'C'),
(10,10,37,'4'),
(10,10,38,'3'),
(10,10,39,'F'),
(10,10,40,'A'),
(11,11,1,'3'),
(11,11,2,'2'),
(11,11,3,'F'),
(11,11,4,'A'),
(12,12,5,'2'),
(12,12,6,'5'),
(12,12,7,'F'),
(12,12,8,'D'),
(13,13,9,'2'),
(13,13,10,'5'),
(13,13,11,'F'),
(13,13,12,'D'),
(14,14,13,'2'),
(14,14,14,'3'),
(14,14,15,'T'),
(14,14,16,'A'),
(15,15,15,'T'),
(15,15,16,'A'),
(15,15,17,'2'),
(15,15,18,'5');

-- Survey description
-- Y relevant constraints
drop table if exists survey;
CREATE TABLE survey (
id serial primary key,
title text,
description text,
author int
);

-- Y descriptive statistics for all-the-things, e.g. counts of total surveys, active surveys, users (by survey, by question, etc), questions (by survey, by user, etc)
-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
-- Y 5 surveys (at least 4 questions each)
-- Y +5 surveys (at least 4 questions each)
-- Y +5 surveys (at least 4 questions each)
insert into survey(id, title, description, author) values
(1, 'Purple', 'short description', 1),
(2, 'Orange', 'Longer description', 1),
(3, 'Untitled Survey', 'Insert a description here', 1),
(4, 'ACU Trivia', 'How well do you know ACU?!?!?1', 2),
(5, 'Car Survey', 'Tell us about your car!', 2),
(6, 'Car Survey part deux', 'Tell us about you car... But differently this time', 2),
(7, 'CSS Quiz', 'CSS Quiz', 3),
(8, 'JS Quiz', 'JS Quiz', 3),
(9, 'Class Eval', 'How well did your professor do this semester', 3),
(10, 'Random Trivia', 'How much useless stuff do you know', 4),
(11, 'Likert Survey', 'Template survey for Likert Questions', 4),
(12, 'Untitled Survey(2)', 'Insert a description here', 4),
(13, 'Colors Survey', 'Colors? Colors. Colors.', 1),
(14, 'Untitled Survey(1)', 'Insert a description here', 2),
(15, 'FIXED ACU Trivia', 'Ignore the last survey you received. It was broken.', 3);

-- Y relevant constraints
drop table if exists users;
CREATE TABLE users (
  id int primary key,
  email text,
  password text
);

-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
-- Y 20 users (15 do 1 survey)
-- Y 20 users (5 do 2 surveys)
-- Y +10 users (5 do 4 surveys)
insert into users(id, email, password) values
(1, 'acu@acu.edu', 'Password1'),
(2, 'bsu@hsu.edu', '123456!!!'),
(3, 'theRealShuub@acu.edu', 'NeverGnnaGiveUUP'),
(4, 'timmy.tommy.tammy@gmail.com', 't1mt0mTaM'),
(5, 'Johnathan.Johnathan@acu.edu', 'PAssword!'),
(6, 'Mr.HSU@hsu.edu', 'APplesRus'),
(7, 'akj95x@acu.edu', 'aSjvo14!^%'),
(8, 'timmy.timmy.timmy@gmail.com', 'timTimTIM'),
(9, 'bnr01a@acu.edu', 'bleeblah'),
(10, 'yodelingspider@gmail.com', 'passw0rd'),
(11, 'trs42a@acu.edu', 'Password1'),
(12, 'jimmyjohns@gmail.com', 'Jimmyj0hns#ast'),
(13, 'acu06a@acu.edu', '1drowssaP'),
(14, 'brh00@acu.edu', '78910!!!'),
(15, 'lxo@acu.edu', 'NeverGnnaGive'),
(16, 'timmy.tommy.tammy@aol.com', 't1mt0mTaM'),
(17, 'bos16a@acu.edu', '!drowssaPassword!'),
(18, 'BillySprinkleCakes@yahoo.com', '05/19/98'),
(19, 'sammy.clamsworth@acu.edu', '111!!!111!!!'),
(20, 'jimmy.jommy.jammy@gmail.com', 'j1mj0mJaM'),
(21, 'ncy20x@acu.edu', 'Password1234!'),
(22, 'vou15i@acu.edu', '123456!!!'),
(23, 'mrGoogle@gmail.com', '$e11ingUrDat4'),
(24, 'ttt05a@acu.edu', 't1mt0mTaM'),
(25, 'señorshuub@gmail.com', 'NeverGnnaGiveUUP'),
(26, 'dum15a@acu.edu', 'asd;lkj19'),
(27, 'dummyHoy@gmail.com', 'GiveUP!!!'),
(28, 'timmy.tommy.tammy@vivaldi.net', 't1mt0mTaM'),
(29, 'acu@gmail.com', 'Password1'),
(30, 'bsu@yahoo.com', '123456!!!'),
(31, 'txl01@acu.edu', '123456abcdef'),
(32, 'timmy.tommy.tammy@yahoo.com', 't1mt0mTaM'),
(33, 'acu@acu.edu', 'Password1'),
(34, 'bsu@hsu.edu', '123456!!!'),
(35, 'theRealShuub@yahoo.edu', 'NeverGnnaGiveUUP'),
(36, 'timmy.tommy.tammy@hotmail.com', 't1mt0mTaM'),
(37, 'bartledoo@donkey.com', '#1ShrekFan2001'),
(38, 'hsu@hsu.edu', '123456!!!'),
(39, 'thefakeShuub@gmail.com', 'NeverGnnaGiveUUP'),
(40, 'mr.bean@gmail.com', 'ThankGodImD0ne'),
(41, 'mem19b@acu.edu','ErenJaegar');
-- Last one is my Identification to make my sql unique from my partner

-- Author (Author and Administrators are the same)
-- Admin was changed to "Author"
-- Y relevant constraints
drop table if exists author;
CREATE TABLE author (
id int primary key,
fn text,
ln text,
email text,
password text,
active char(1)
);

-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
-- +4 Authors (or "Admin")
insert into author(id, fn, ln, email,password, active) values
(1, 'Phil', 'Shubert', 'Phil.Shubert@acu.edu','boss', 'Y'),
(2, 'Phil', 'Swift', 'mrSwift@flexseal.com','boss', 'Y'),
(3, 'Dr.', 'Phil', 'drphil@drphil.com','boss','Y'),
(4, 'Phil', 'Nye', 'thescienceguy@gmail.com','boss', 'Y'),
(5, 'Eren', 'Jaeger','eldia@gmail.com','boss','N');

--Identification per Survey
-- Y relevant constraints
drop table if exists user_surveys;
CREATE TABLE user_surveys (
  user_id int,
  survey_id int
);

-- Y +10 users (15 do 1 survey)
-- Y +10 users (10 do 2 surveys)
-- Y +10 users (5 do 3 surveys)
-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
insert into user_surveys (user_id, survey_id) values
(1,1),
(2,2),
(3,3),
(4,4),
(5,5),
(6,6),
(7,7),
(8,8),
(9,9),
(10,10),
(11,11),
(12,12),
(13,13),
(14,14),
(15,15),
(16,16),
(16,17),
(17,17),
(17,18),
(18,18),
(18,19),
(19,19),
(19,20),
(20,20),
(20,21),
(21,21),
(21,22),
(22,22),
(22,23),
(23,23),
(23,24),
(24,24),
(24,25),
(25,25),
(25,26),
(26,26),
(26,27),
(27,27),
(27,28),
(28,28),
(28,29),
(29,29),
(30,30),
(30,31),
(31,30),
(31,31),
(31,32),
(32,31),
(32,32),
(32,33),
(33,32),
(33,33),
(33,34),
(34,33),
(34,34),
(34,35),
(35,34),
(35,35),
(35,36),
(36,34),
(36,35),
(36,36),
(36,37),
(37,35),
(37,36),
(37,37),
(37,38),
(38,36),
(38,37),
(38,38),
(38,39),
(39,37),
(39,38),
(39,39),
(39,40),
(40,37),
(40,38),
(40,39),
(40,40);

-- Survey Question/User
-- Y relevant constraints
drop table if exists survey_question;
CREATE TABLE survey_question (
  survey_id int,
  question_id int
);

-- Y insert statements for each table and selects 'proving' it works
-- Y sample data
-- Y 5 surveys (at least 4 questions each)
-- Y +5 surveys (at least 4 questions each)
-- Y +5 surveys (at least 4 questions each)
-- Also used in response to determine how many questions each survey has
insert into survey_question(survey_id, question_id) values
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(3, 9),
(3,10),
(3,11),
(3,12),
(4,13),
(4,14),
(4,15),
(4,16),
(5,17),
(5,18),
(5,19),
(5,20),
(6,21),
(6,22),
(6,23),
(6,24),
(7,25),
(7,26),
(7,27),
(7,28),
(8,29),
(8,30),
(8,31),
(8,32),
(9,33),
(9,34),
(9,35),
(9,36),
(10,37),
(10,38),
(10,39),
(10,40),
(11,1),
(11,2),
(11,3),
(11,4),
(12,5),
(12,6),
(12,7),
(12,8),
(13,9),
(13,10),
(13,11),
(13,12),
(14,13),
(14,14),
(14,15),
(14,16),
(15,15),
(15,16),
(15,17),
(15,18);

-- Y insert statements for each table and selects 'proving' it works
-- All below joining each table
select * from author;
-- Standard True and False, Likert 4 and Likert 5 questions
select Q.id, Q.text as Questions, Q.question_type, CT.choice from question Q
inner join choices_Type CT on (CT.question_type = Q.question_type)
order by Q.id, CT.choice;

-- Multiple Choice Questions and their Multiple Choice Answers A-D Top to Bottom
select Q.id, Q.text as Multiple_Choice_Questions, mc.choices as Multiple_Choice_Possibilities from question Q
inner join choice_mc mc on (mc.id_q_n = Q.id)
order by Q.id;

-- Survey per User and Question per Survey
select us.user_id, us.survey_id, s.question_id from user_surveys us
inner join survey_question s on (s.survey_id = us.survey_id)
order by us.user_id;

-- Authors and what Surveys they have created
select a.fn, a.ln, s.id, s.title, s.description, a.active from survey s
inner join author a on (a.id = s.author)
order by s.id;

--Authors and their active or inactive state
select a.fn, a.ln, a.active from author a order by a.fn;

--Questions with respective users, type of question and the answers
select r.user_id as user, q.id as question_id, q.text as question, q.question_type, r.answer from question q
inner join response r on (q.id = r.question_id)
order by r.user_id, q.id, r.answer;
