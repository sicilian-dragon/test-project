const addButton = document.querySelector(".Add-Submit-Button");
// newQuestion.classList.add("question_selection");
// newQuestion.innerHTML = Question;

// lastQuestion.append(newQuestion);

//add new question when button is pressed
addButton.addEventListener("click", (event) => {
  const lastQuestion = document.querySelector(".lastQuestion");
	const newQuestion = document.createElement("div");
	newQuestion.classList.add("question_selection");
	newQuestion.innerHTML =`
      <label for="question-type">Type of Question:</label>

      	<select name="question-type" class="question-type">
         <option>Select a Question Type</option>
        <option>Multiple Choice</option>
        <option>True or False (T/F)</option>
        <option>Likert 4</option>
        <option>Likert 5</option>
       </select>

      <p><textarea rows="2" placeholder="Enter your question"></textarea></p>

`;

	lastQuestion.insertAdjacentElement('afterend', newQuestion);
	lastQuestion.classList.remove("lastQuestion");
	newQuestion.classList.add("lastQuestion");

});
const selection = document.querySelectorAll(".question-type");
selection.forEach((quest) =>{
   quest.addEventListener("change", (event) => {
      const question = document.querySelector(".question_selection");
      if(`${event.target.value}` === "Multiple Choice")
      {
         console.log(`${event.target.value}`);
         const mC = document.createElement("div");
         mC.classList.add("options");
         mC.innerHTML = `<p>
                     <input type="radio">
                     <textarea placeholder="Option 1"></textarea>
                  </p>
                  <p>
                     <input type="radio">
                     <textarea placeholder="Option 2"></textarea>
                  </p>
                  <p>
                     <input type="radio">
                     <textarea placeholder="Option 3"></textarea>
                  </p>
                  <p>
                     <input type="radio">
                     <textarea placeholder="Option 4"></textarea>
                  </p>
         `;
         question.insertAdjacentElement('beforeend', mC);

       } else if(`${event.target.value}` === "True or False (T/F)")
         {
            console.log(`${event.target.value}`);
            const tF = document.createElement("div");
            tF.classList.add("options");
            tF.innerHTML = `<p>
                        <input type="radio">
                        <textarea placeholder="True"></textarea>
                     </p>
                     <p>
                        <input type="radio">
                        <textarea placeholder="False"></textarea>
                     </p>
            `;
            question.insertAdjacentElement('beforeend', tF);

         } else if(`${event.target.value}` === "Likert 4")
         {
            console.log(`${event.target.value}`);
            const l4 = document.createElement("div");
            l4.classList.add("options");
            l4.innerHTML = `<p>
                        <input type="radio" name="1">
                        <label for="1">Strongly Disagree</label>
                     </p>
                        <input type="radio" name="2">
                        <label for="1">Disagree</label>
                     </p>
                     </p>
                        <input type="radio" name="4">
                        <label for="1">Agree</label>
                     </p>       
                     </p>
                        <input type="radio" name="5">
                        <label for="1">Strongly Agree</label>
                     </p>              
            `;
            question.insertAdjacentElement('beforeend', l4);

         } else if(`${event.target.value}` === "Likert 5")
         {
            console.log(`${event.target.value}`);
            const l5 = document.createElement("div");
            l5.classList.add("options");
            l5.innerHTML = `<p>
                        <input type="radio" name="1">
                        <label for="1">Strongly Disagree</label>
                     </p>
                        <input type="radio" name="2">
                        <label for="1">Disagree</label>
                     </p>
                     </p>
                        <input type="radio" name="3">
                        <label for="1">Neutral</label>
                     </p> 
                     </p>
                        <input type="radio" name="4">
                        <label for="1">Agree</label>
                     </p>       
                     </p>
                        <input type="radio" name="5">
                        <label for="1">Strongly Agree</label>
                     </p>              
            `;
            question.insertAdjacentElement('beforeend', l5);

         }

   });
});
